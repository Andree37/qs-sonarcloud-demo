const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Lets make a demo!')
})

app.get('/demo', (req, res) => {
    //this is gonna give some warnings on sonar based on the quality gate
       var a = 2
       var a = 5 
       a = 3
        res.send(Math.floor(Math.random() * Math.floor(100)).toString())
  })

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
